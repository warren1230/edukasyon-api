Installation steps:

1. Make sure to have MySQL Server up and running.
2. Perform rake db:create in the project console
3. Perform rails db:migrate
4. Run the server: rails server