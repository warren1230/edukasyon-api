Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users
  resources :courses
  resources :professors
  resources :professor_classes
  resources :professor_classes_professors
  resources :reviews
end
