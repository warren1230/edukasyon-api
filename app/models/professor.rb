class Professor < ApplicationRecord
    has_and_belongs_to_many :professor_classes
    has_many :reviews , dependent: :destroy
end
