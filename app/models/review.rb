class Review < ApplicationRecord
    belongs_to :professor, optional: true
end
