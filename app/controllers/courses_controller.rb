class CoursesController < ApplicationController
    #GET /courses
    def index
        @courses = Course.all
        json_response(@courses)
    end

    #POST /courses
    def create
        @course = Course.create!(course_params)
        json_response(@course, :created)
    end

    #GET /courses/:id
    def show
        @course = Course.find(params[:id])
        json_response(@course)
    end

    #PUT /courses/:id
    def update
        @course = Course.find(params[:id])
        if @course
            @course.update(course_params)
            head :no_content
        end
    end

    #DELETE /courses/:id
    def destroy
        @course = Course.find(params[:id])
        if @course
            @course.destroy
            head :no_content
        end
    end

    private

    def course_params
        params.permit(:name)
    end
end
