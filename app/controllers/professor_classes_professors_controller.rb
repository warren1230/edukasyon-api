class ProfessorClassesProfessorsController < ApplicationController
    #GET /professor_classes_professors
    def index
        if params[:professor_class_id]
            @professors = Professor.joins(:professor_classes).where('professor_classes_professors.professor_class_id = ?', params[:professor_class_id])
            # @professors = ActiveRecord::Base.connection.execute('SELECT professors.id, professors.name, avg(reviews.rating) FROM `professors` INNER JOIN `professor_classes_professors` ON `professor_classes_professors`.`professor_id` = `professors`.`id` INNER JOIN `professor_classes` ON `professor_classes`.`id` = `professor_classes_professors`.`professor_class_id` INNER JOIN `reviews` ON `reviews`.`professors_id` = `professors`.`id` WHERE professor_class_id = 1 ');
        else
            @professors = Professor.joins(:professor_classes).where('professor_id = ?', params[:professor_id]);
        end
        json_response(@professors)
    end

    #POST /professor_classes_professors
    def create
        @professor = Professor.find(params[:professor_id])
        @professor_classes = ProfessorClass.find(params[:professor_class_id])
        @professor_classes_professor = ProfessorClassesProfessor.new(params[:professor_classes_professors])
        @professor_classes_professor = ProfessorClassesProfessor.create!(professor_classes_professor_params)
        json_response(@professor_classes_professor, :created)
    end

    #GET /professor_classes_professors/:id
    def show
        @professor_classes_professors = ProfessorClassProfessors.find(params[:id])
        json_response(@professor_classes_professors)
    end

    #PUT /professor_classes_professors/:id
    def update
        @professor_classes_professor = ProfessorClassesProfessor.find(params[:id])
        if @professor_classes_professor
            @professor_classes_professor.update(professor_classes_professor_params)
            head :no_content
        end
    end

    #DELETE /professor_classes_professors/:id
    def destroy
        @professor_class = ProfessorClassesProfessor.find(params[:id])
        if @professor_class
            @professor_class.destroy
            head :no_content
        end
    end

    private

    def professor_classes_professor_params
        params.permit(:professor_id, :professor_class_id)
    end
end
