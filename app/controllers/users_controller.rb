class UsersController < ApplicationController

    #GET /users
    def index
        @users = User.all
        json_response(@users)
    end

    #POST /users
    def create
        @user = User.create!(user_params)
        json_response(@user, :created)
    end

    #GET /users/:id
    def show
        @user = User.find(params[:id])
        json_response(@user)
    end

    #PUT /users/:id
    def update
        @user.update(user_params)
        head :no_content
    end

    #DELETE /users/:id
    def destroy
        @user.destroy
        head :no_content
    end

    private

    def user_params
        params.permit(:username, :password)
    end
end
