class ProfessorsController < ApplicationController

    #GET /professors
    def index
        @professors = Professor.all
        json_response(@professors)
    end

    #POST /professors
    def create
        @professor = Professor.create!(professor_params)
        json_response(@professor, :created)
    end

    #GET /professors/:id
    def show
        @professor = Professor.find(params[:id])
        json_response(@professor)
    end

    #PUT /professors/:id
    def update
        @professor = Professor.find(params[:id])
        if @professor
            @professor.update(professor_params)
            head :no_content
        end
    end

    #DELETE /professors/:id
    def destroy
        @professor = Professor.find(params[:id])
        if @professor
            @professor.destroy
            head :no_content
        end
    end

    private

    def professor_params
        params.permit(:name)
    end
end
