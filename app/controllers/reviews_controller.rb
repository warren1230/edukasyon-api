class ReviewsController < ApplicationController

    #GET /reviews
    def index
        if params[:professors_id]
            @reviews = Review.where('reviews.professors_id = ?', params[:professors_id]);
        else
            @reviews = Review.all
        end
        json_response(@reviews)
    end

    #POST /reviews
    def create
        @review = Review.new(params[:reviews])
        @review = Review.create!(review_params)
        json_response(@review, :created)
    end

    #GET /reviews/:id
    def show
        @review = Review.find(params[:id])
        json_response(@review)
    end

    #PUT /reviews/:id
    def update
        @review = Review.find(params[:id])
        if @review
            @review.update(review_params)
            head :no_content
        end
    end

    #DELETE /review/:id
    def destroy
        @review = Review.find(params[:id])
        if @review
            @review.destroy
            head :no_content
        end
    end

    private

    def review_params
        params.permit(:rating, :description, :professors_id)
    end
end
