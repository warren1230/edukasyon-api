class ProfessorClassesController < ApplicationController
    #GET /professor_classes
    def index
        @professor_classes = ProfessorClass.all
        json_response(@professor_classes)
    end

    #POST /professor_classes
    def create
        @professor_class = ProfessorClass.create!(professor_class_params)
        json_response(@professor_class, :created)
    end

    #GET /professor_classes/:id
    def show
        @professor_class = ProfessorClass.find(params[:id])
        json_response(@professor_class)
    end

    #PUT /professor_classes/:id
    def update
        @professor_class = ProfessorClass.find(params[:id])
        if @professor_class
            @professor_class.update(professor_class_params)
            head :no_content
        end
    end

    #DELETE /professor_classes/:id
    def destroy
        @professor_class = ProfessorClass.find(params[:id])
        if @professor_class
            @professor_class.destroy
            head :no_content
        end
    end

    private

    def professor_class_params
        params.permit(:name)
    end
end
