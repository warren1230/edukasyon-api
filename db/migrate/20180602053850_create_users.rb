class CreateUsers < ActiveRecord::Migration[5.1]
  def self.up
      create_table :users do |t|
        t.column :username, :string
        t.column :password, :string

        t.timestamps
      end
   end

   def self.down
      drop_table :users
   end
end
