class CreateProfessorClasses < ActiveRecord::Migration[5.1]
  def change
    create_table :professor_classes do |t|
      t.string :name
      t.timestamps
    end
  end
end
