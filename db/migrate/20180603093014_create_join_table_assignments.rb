class CreateJoinTableAssignments < ActiveRecord::Migration[5.1]
  def change
    create_join_table :professor_classes, :professors do |t|
      # t.index [:professor_class_id, :professor_id]
      # t.index [:professor_id, :professor_class_id]
    end
  end
end
