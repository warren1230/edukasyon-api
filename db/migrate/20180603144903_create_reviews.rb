class CreateReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :reviews do |t|
      t.integer :rating
      t.string :description

      t.timestamps
    end

    add_reference :reviews, :professors, index:true, foreign_key: true
    add_foreign_key :reviews, :professors, column: :id
  end
end
